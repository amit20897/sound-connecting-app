package com.berg.green.soundconnectingapp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class MelodyPart {

    //Constants
    public static final String PART_NAME_DURATION_SEPERATOR = ":";

    //Parameters
    private String soundName;
    private float soundDuration;


    //Initialization
    public MelodyPart(@NonNull String soundName, float soundDuration) {
        this.soundName = soundName;
        this.soundDuration = soundDuration;
    }

    @Nullable
    public static MelodyPart fromString(String melodyPartStr) {

        int indexOfSeparator = melodyPartStr.indexOf(PART_NAME_DURATION_SEPERATOR);
        if (indexOfSeparator != -1) {
            try {
                String soundName = melodyPartStr.substring(0, indexOfSeparator);
                float soundDuration = Float.parseFloat(melodyPartStr.substring(indexOfSeparator + 1));

                return new MelodyPart(soundName, soundDuration);
            } catch (NumberFormatException e) {
                e.printStackTrace();

                return null;
            }
        }

        return null;
    }


    //Getters & Setters
    public String getSoundName() {
        return soundName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }

    public float getSoundDuration() {
        return soundDuration;
    }

    public void setSoundDuration(float soundDuration) {
        this.soundDuration = soundDuration;
    }
}
