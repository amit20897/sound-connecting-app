package com.berg.green.soundconnectingapp;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.berg.green.soundconnectingapp.SoundFile.SoundFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Parameters
    private HashMap<String, File> loadedSounds = new HashMap<>();

    //UI Objects
    private Button playMelodyBtn;

    //Initialization
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Connecting views
        this.playMelodyBtn = findViewById(R.id.play_melody_btn);

        //Settings click listeners
        this.playMelodyBtn.setOnClickListener(this);
    }


    //Private functions
    private File createWavFileFromMelody(String melodyStr) throws IOException, SoundFile.InvalidInputException {
        Melody melody = new Melody(melodyStr);

        File finalMelodyWav = File.createTempFile("finalMelody", ".wav");

        for (MelodyPart melodyPart : melody.getMelodyParts()) {

            int resourceIdentifier = getResources().getIdentifier(melodyPart.getSoundName(), "raw", getPackageName());
            if (resourceIdentifier != 0) {
                File melodyPartSound;

                if (loadedSounds.containsKey(melodyPart.getSoundName())) {
                    melodyPartSound = loadedSounds.get(melodyPart.getSoundName());
                } else {
                    melodyPartSound = createTempFileFromResource(melodyPart.getSoundName(), resourceIdentifier);
                    loadedSounds.put(melodyPart.getSoundName(), melodyPartSound);
                }

                appendSoundWithMelodyPart(melodyPart, finalMelodyWav, melodyPartSound);
            }
        }

        return finalMelodyWav;
    }

    private void appendSoundWithMelodyPart(MelodyPart melodyPart, File toSound, File melodyPartSound) throws IOException, SoundFile.InvalidInputException {

        final SoundFile soundFile = SoundFile.create(melodyPartSound.getPath(), null);

        if (soundFile != null) {
            //TODO make file appending instead of replacing
            soundFile.WriteWAVFile(toSound, 0f, melodyPart.getSoundDuration());
        }
    }

    private File createTempFileFromResource(String resourceName, int resource) throws IOException {
        InputStream inStream = getResources().openRawResource(resource);
        byte[] soundBytes = convertStreamToByteArray(inStream);

        final File tempFile = File.createTempFile("sound" + resourceName, ".wav");

        tempFile.deleteOnExit();
        FileOutputStream fos = new FileOutputStream(tempFile);
        fos.write(soundBytes);
        fos.close();

        return tempFile;
    }

    private byte[] convertStreamToByteArray(InputStream inStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int reads = inStream.read();

        while (reads != -1) {
            baos.write(reads);
            reads = inStream.read();
        }

        return baos.toByteArray();
    }

    private void playSound(File soundFile) throws IOException {
        if (soundFile.exists()) {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(soundFile.getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.play_melody_btn:

                String melody = "G4:0.75 C4:0.75 Eb4:0.125 F4:0.125 G4:0.5 C4:0.5 Eb4:0.125 F4:0.125 D4:3 F4:0.75 Bb3:0.75 Eb4:0.125 D4:0.125 F4:0.5 Bb3:0.75 Eb4:0.125 D4:0.125 C4:2.75 G4:0.75 C4:0.75 Eb4:0.125 F4:0.125 G4:0.5 C4:0.5 Eb4:0.125 F4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 F4:0.75 Bb3:0.75 Eb4:0.125 D4:0.125 F4:0.5 Bb3:0.75 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 G4:0.75 C4:0.75 Eb4:0.125 F4:0.125 G4:0.5 C4:0.5 Eb4:0.125 F4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 D4:0.25 G3:0.25 Bb3:0.125 C4:0.125 F4:0.75 Bb3:0.75 D4:0.375 Eb4:0.375 D4:0.375 Bb3:0.375 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C5:1.25 Bb4:0.125 C5:0.125 Bb4:1.5 Ab4:1.25 G4:0.125 Ab4:0.125 G4:1.5 Eb4:1.5 Eb4:0.75 F4:0.75 G4:3 C5:0.25 Eb4:0.25 Ab4:0.125 Bb4:0.125 C5:0.25 Eb4:0.25 Ab4:0.125 C5:0.125 Bb4:0.25 Eb4:0.25 G4:0.125 Ab4:0.125 Bb4:0.25 Eb4:0.25 G4:0.125 Bb4:0.125 Ab4:0.25 C4:0.25 F4:0.125 G4:0.125 Ab4:0.25 C4:0.25 F4:0.125 Ab4:0.125 G4:0.25 C4:0.25 Eb4:0.125 F4:0.125 G4:0.25 C4:0.25 Eb4:0.125 F4:0.125 Eb4:0.25 Ab3:0.25 C4:0.125 D4:0.125 Eb4:0.25 Ab3:0.25 C4:0.125 D4:0.125 Eb4:0.25 Bb3:0.25 D4:0.125 Eb4:0.125 F4:0.25 Bb3:0.25 Eb4:0.125 F4:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125 C4:0.25 G3:0.25 Ab3:0.125 Bb3:0.125";

                try {
                    File wavFile = createWavFileFromMelody(melody.toLowerCase());

                    playSound(wavFile);
                } catch (IOException | SoundFile.InvalidInputException e) {
                    e.printStackTrace();

                    Toast.makeText(this, "failed creating wav file", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}
