package com.berg.green.soundconnectingapp;

import java.util.ArrayList;

public class Melody {

    //Constants
    public static final String MELODY_PART_SEPARATOR = " ";

    //Parameters
    private ArrayList<MelodyPart> melodyParts;


    //Initialization
    public Melody(String melody) {
        this.melodyParts = createMelodyFromString(melody);
    }


    //Private functions
    private ArrayList<MelodyPart> createMelodyFromString(String melody) {
        ArrayList<MelodyPart> melodyParts = new ArrayList<>();

        for (String melodyPartStr : melody.split(MELODY_PART_SEPARATOR)) {
            MelodyPart melodyPart = MelodyPart.fromString(melodyPartStr);

            melodyParts.add(melodyPart);
        }

        return melodyParts;
    }


    //Getters & Setters

    public ArrayList<MelodyPart> getMelodyParts() {
        return melodyParts;
    }
}
